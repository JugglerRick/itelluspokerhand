﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerDeck
{
    /// <summary>
    /// Represents a playing card
    /// </summary>
    /// <remarks>
    /// A card is a unique member of a NUMBER_OF_CARDS card deck. The deck contains 4 suites
    /// diamonds, clubs, hearts, and spades which are ranked in that order.
    /// Each suite contains 13 cards, 2,3,4,5,6,7,8,9,10,Jack, Queen, King, and Ace,
    /// 
    /// </remarks>
    public class Card : IComparable<Card>, IComparable<Card.Values>, IComparable<Card.Suites>
    {
        public const int NUMBER_OF_VALUES = 13;
        public enum Values  { Two = 0, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace };
        public const int NUMBER_OF_SUITES = 4;
        public enum Suites { Diamonds, Clubs, Hearts, Spades };
        public const int NUMBER_OF_CARDS = 52;
        
        /// <summary>
        /// The card Rank  
        /// </summary>
        private int rank;
        public int Rank 
        {
            get { return rank; }
            private set { rank = value % NUMBER_OF_CARDS; } //insure the rank is within a single deck
        }

        /// <summary>
        /// The value of the card
        /// </summary>
        public Values Value
        {
            get
            {
                return (0 == Rank) ? Values.Two : (Values)(Rank / NUMBER_OF_SUITES); 
            }
        }

        /// <summary>
        /// The suite of the card
        /// </summary>
        public Suites Suite
        {
            get
            {
                return (0 == Rank) ? Suites.Diamonds : (Suites)(Rank % NUMBER_OF_SUITES);
            }
        }

        /// <summary>
        /// Initializing constructor
        /// </summary>
        /// <param name="id">The ID of the card in the deck</param>
        public Card(int id)
        {
            Rank = id;
        }

        /// <summary>
        /// convert to string override
        /// </summary>
        /// <returns>a string of the card</returns>
        public override string ToString()
        {
            return string.Format("{0} of {1}", Value, Suite);
        }
    

        #region IComparable<Card> Members

        public int CompareTo(Card other)
        {
            return Rank.CompareTo(other.Rank);
        }

        public int CompareTo(int other)
        {
            return Rank.CompareTo(other);
        }

        #endregion

        #region IComparable<Values> Members

        public int CompareTo(Card.Values other)
        {
            return Value.CompareTo(other);
        }

        #endregion

        #region IComparable<Suites> Members

        public int CompareTo(Card.Suites other)
        {
            return Suite.CompareTo(other);
        }

        #endregion
    }
}
