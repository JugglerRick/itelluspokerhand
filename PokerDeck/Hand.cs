﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerDeck
{
    public class Hand : IComparable<Hand>
    {
        public const int NUMBER_OF_CARDS = 5;
        /// <summary>
        /// The list of card in the hand sorted by Card.Rank
        /// </summary>
        private SortedList<int, Card> cards;
        public IList<Card> Cards
        {
            get { return cards.Values; }
        }

        // these are used to analysis the rank of the cards
        private SortedDictionary<Card.Values, List<Card>> valueDir;
        private SortedDictionary<Card.Suites, List<Card>> suiteDir;

        /// <summary>
        /// default constructor is hidden
        /// </summary>
        public Hand() 
        {
            cards = new SortedList<int, Card>();
            valueDir = new SortedDictionary<Card.Values, List<Card>>();
            suiteDir = new SortedDictionary<Card.Suites, List<Card>>();
        }

        /// <summary>
        /// Instance Constructor
        /// </summary>
        /// <param name="hand"> A list of NUMBER_OF_CARDS Card objects that makeup the hand</param>
        public Hand(IEnumerable<Card> hand) : this()
        {
            if (NUMBER_OF_CARDS != hand.Count())
            {
                throw new ArgumentOutOfRangeException(string.Format("A Hand must have {0} Cards in it", NUMBER_OF_CARDS));
            }
            foreach (Card card in hand)
            {
                AddCard(card);
            }
        }

        /// <summary>
        /// Add a card to the hand
        /// </summary>
        /// <param name="card"></param>
        public void AddCard(Card card)
        {
            if (NUMBER_OF_CARDS == cards.Count)
            {
                throw new ArgumentOutOfRangeException(string.Format("The Hand already contains {0} Cards in it", NUMBER_OF_CARDS));
            }
            cards.Add(card.Rank, card);
            if (!valueDir.ContainsKey(card.Value))
            {
                valueDir.Add(card.Value, new List<Card>());
            }
            valueDir[card.Value].Add(card);

            if (!suiteDir.ContainsKey(card.Suite))
            {
                suiteDir.Add(card.Suite, new List<Card>());
            }
            suiteDir[card.Suite].Add(card);
        }

        /// <summary>
        /// Clear the hand
        /// </summary>
        public void Fold()
        {
            cards.Clear();
            valueDir.Clear();
            suiteDir.Clear();
        }


        /// <summary>
        /// Override to convert to a string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string ret = "Hand is invalid";
            if (IsValid())
            {
                ret = string.Empty;
                foreach (Card card in cards.Values)
                {
                    ret += $"{card.ToString()} "; 
                }
            }

            return ret;
        }

        /// <summary>
        /// Test if this is valid hand
        /// </summary>
        /// <returns>true if this hand contains NUMBER_OF_CARDS card</returns>
        public bool IsValid()
        {
            return (NUMBER_OF_CARDS == cards.Count());
        }

        /// <summary>
        /// Find the highest value card
        /// </summary>
        /// <returns>the Card with the highest value in the hand</returns>
        public Card HighestValue()
        {
            return cards.Last().Value;
        }

        /// <summary>
        /// Find the lowest value card
        /// </summary>
        /// <returns>The Card with the lowest value int the hand</returns>
        public Card LowestValue()
        {
            return cards.First().Value;
        }

        /// <summary>
        /// Find the highest card of the highest pair
        /// </summary>
        /// <returns>the highest card of the highest pair</returns>
        private Card FindHighPairCard()
        {
            Card ret = null;
            Card second = null;

            foreach (var key in valueDir.Keys)
            {
                if (1 < valueDir[key].Count)
                {
                    if(null == ret)
                        ret = valueDir[key].Max();
                    else
                        second = valueDir[key].Max();
                }
            }
            if(null != second && second.Rank > ret.Rank)
            {
                ret = second;
            }
            return ret;
        }

        /// <summary>
        /// Find the lowest pair/threeofakind in the hand
        /// </summary>
        /// <returns>The lowest card of the lowest pair</returns>
        private Card FindLowPairCard()
        {
            Card ret = null;
            Card second = null;

            foreach (var key in valueDir.Keys)
            {
                if (1 < valueDir[key].Count)
                {
                    if(null == ret)
                        ret = valueDir[key].Min();
                    else
                        second = valueDir[key].Min();
                }
            }
            if(null != second && second.Rank < ret.Rank)
            {
                ret = second;
            }
            return ret;
        }

        /// <summary>
        /// Find the highest card in the hand that is not pair of a pair/threeofkind/fourofkind
        /// </summary>
        /// <returns>the highest card in the hand</returns>
        private Card FindHighNonPairCard()
        {
            Card ret = null;
            foreach (var key in valueDir.Keys)
            {
                if (1 == valueDir[key].Count)
                {
                    if (null == ret || ret.Rank < valueDir[key].First().Rank)
                        ret = valueDir[key].First();
                }
            }

            return ret;
        }

        /// <summary>
        /// The type of hand and its ranking
        /// </summary>
        public enum HandType { High = 0, Pair, TwoPair, ThreeOfKind, Straight, Flush, FullHouse, FourOfKind, StraightFlush }
        public HandType Rank
        {
            get
            {
                if (!IsValid())
                {
                    throw new InvalidOperationException("The Hand does not contain enough cards to determine Rank");
                }
                HandType ret = HandType.High;

                if (IsFlush())
                {
                    ret = HandType.Flush;
                    if (IsStraight())
                        ret = HandType.StraightFlush;
                }
                else if (IsStraight())
                {
                    ret = HandType.Straight;
                }
                // if there are 4 different values then there must be a pair
                else if (4 == valueDir.Keys.Count())
                {
                    ret = HandType.Pair;
                }
                // if there are 3 different values then there is either 2 pair or 3 of a kind
                else if (3 == valueDir.Keys.Count)
                {
                    foreach (var key in valueDir.Keys)
                    {
                        if (3 == valueDir[key].Count)
                        {
                            ret = HandType.ThreeOfKind;
                        }
                        else if (2 == valueDir[key].Count)
                        {
                            ret = HandType.TwoPair;
                        }

                    }
                }
                // if there are 2 different values then there is either 4 of a kind or a full house
                else if (2 == valueDir.Keys.Count)
                {
                    int firstCount = valueDir[valueDir.Keys.First()].Count;
                    if (1 == firstCount || 4 == firstCount)
                        ret = HandType.FourOfKind;
                    else
                        ret = HandType.FullHouse;
                }

                return ret;
            }
        }

        /// <summary>
        /// Test if the hand is a Straight
        /// </summary>
        /// <returns>
        /// True if this hand is a straight, else false
        /// </returns>
        public bool IsStraight()
        {
            bool ret = IsValid();
            if(ret)
            {
                int dest = HighestValue().Value - LowestValue().Value;
                ret = 4 == dest;
            }
            return ret;  
        }

        /// <summary>
        /// Test if this hand is a Flush
        /// </summary>
        /// <returns>
        /// True if this hand is a Flush
        /// </returns>
        public bool IsFlush()
        {
            return 1 == suiteDir.Keys.Count();
        }

        #region IComparable<Hand> Members
        /// <summary>
        /// Compare 2 hands
        /// </summary>
        /// <param name="other">the other hand</param>
        /// <returns>standard compare return</returns>
        public int CompareTo(Hand other)
        {
            int ret = Rank.CompareTo(other.Rank);
            if (0 == ret)
            {
                switch(Rank)
                {
                    case HandType.Pair:
                    case HandType.ThreeOfKind:
                    case HandType.FourOfKind:
                        {
                            Card pairCard = FindHighPairCard();
                            Card otherPairCard = other.FindHighPairCard();
                            ret = pairCard.CompareTo(otherPairCard.Value);
                            if (0 == ret)
                            {
                                ret = FindHighNonPairCard().CompareTo(other.FindHighNonPairCard());
                            }
                        }
                        break;
                    case HandType.TwoPair:
                    case HandType.FullHouse:
                        {
                            Card pairCard = FindHighPairCard();
                            Card otherPairCard = other.FindHighPairCard();
                            ret = pairCard.CompareTo(otherPairCard.Value);
                            if (0 == ret)
                            {
                                ret = FindLowPairCard().CompareTo(other.FindLowPairCard());
                            }
                            if (0 == ret)
                            {
                                ret = FindHighNonPairCard().CompareTo(other.FindHighNonPairCard());
                            }
                        }

                        break;
                    default:
                        ret = cards.Last().Value.CompareTo(other.cards.Last().Value);
                        break;
                }
            }
            return ret;
        }

        #endregion
    }
}
