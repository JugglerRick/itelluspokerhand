﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerDeck
{
    public class Player : IComparable<Player>, IComparable<Hand>
    {
        public string Name { get; set; }

        private Hand hand;

        public Hand Hand
        {
            get 
            {
                if(null == hand)
                    hand = new Hand();
                return hand;
            }
            private set { hand = value; }
        }

        public Player()
        {
            Name = string.Empty;
            hand = null;
        }
        
        public Player(string name): this()
        {
            Name = name;
        }

        public Player(string name, IEnumerable<Card> cards)
        {
            Name = name;
            Hand = new Hand(cards);
        }



        public void DealtCard(Card card)
        {
            Hand.AddCard(card);
        }


        #region IComparable<Player> Members

        public int CompareTo(Player other)
        {
            return CompareTo(other.Hand);
        }

        #endregion

        #region IComparable<Hand> Members

        public int CompareTo(Hand other)
        {
            return Hand.CompareTo(other);
        }

        #endregion
    }
}
