﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PokerDeck
{
    public class Deck
    {
        public const int NUMBER_OF_CARDS = Card.NUMBER_OF_CARDS;
        /// <summary>
        /// internal card queue
        /// </summary>
        private Queue<Card> cards;
        private Queue<Card> Cards
        {
            get 
            {
                return cards; 
            }
        }
        
        /// <summary>
        /// type constructor
        /// </summary>
        public Deck()
        {
            cards = new Queue<Card>(Build());
        }

        private List<Card> Build()
        {
            List<Card> deck = new List<Card>(NUMBER_OF_CARDS);

            for (int i = 0; i < NUMBER_OF_CARDS; ++i)
            {
                deck.Add(new Card(i % Card.NUMBER_OF_CARDS));
            }
            return deck;
        }

        /// <summary>
        /// Shuffle the deck
        /// </summary>
        public void Shuffle()
        {
            List<Card> sourceDeck = Build();

            sourceDeck = cards.ToList<Card>();
            cards.Clear();

            Random rand = new Random();
            int cardIndex;

            while(sourceDeck.Count > 0)
            {
                cardIndex = rand.Next(sourceDeck.Count);
                cards.Enqueue(sourceDeck[cardIndex]);
                sourceDeck.RemoveAt(cardIndex);
            }
        }

        /// <summary>
        /// Get the card from the top of the deck
        /// </summary>
        /// <returns>the card from the top of the deck</returns>
        public Card DealCard()
        {
            if(Cards.Count == 0)
            {
                throw new ArgumentOutOfRangeException("All cards have been dealt");
            }
            return Cards.Dequeue();
        }

    }
}
