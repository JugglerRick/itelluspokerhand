﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PokerDeck; 


namespace PokerDeckExample
{
    
    class Program
    {
        static void Main(string[] args)
        {
            Player player1 = new Player("Fred");
            Player player2 = new Player("Tom");
            Deck deck = new Deck();

            deck.Shuffle();
            for (int i = 0; i < Hand.NUMBER_OF_CARDS; ++i)
            {
                player1.DealtCard(deck.DealCard());
                player2.DealtCard(deck.DealCard());
            }
            Console.WriteLine($"{player1.Name}'s Hand: {player1.Hand.ToString()} ");
            Console.WriteLine($"{player2.Name}'s Hand: {player2.Hand.ToString()} ");



            string winner = player2.Name;
            if (0 < player1.CompareTo(player2))
            {
                winner = player1.Name;
            }
            Console.WriteLine($"{winner} has won the game");
            Console.ReadKey();
        }
    }
}
