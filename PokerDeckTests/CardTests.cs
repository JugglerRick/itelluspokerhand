﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using PokerDeck;

namespace PokerDeckTests
{
    [TestClass]
    public class CardTests
    {

        /// <summary>
        /// Utility to build a deck
        /// </summary>
        /// <returns></returns>
        private List<Card> BuildDeck()
        {
            List<Card> deck = new List<Card>();
            for (int i = 0; i < Card.NUMBER_OF_CARDS; ++i)
            {
                deck.Add(new Card(i));
            }
            return deck;
        }


        [TestMethod]
        public void CardValueTests()
        {
            Card card = new Card(0);
            Assert.IsTrue(card.Suite == Card.Suites.Diamonds && card.Value == Card.Values.Two, "The first card is not the Two of Diamonds it is: {0}", card.ToString());
            card = new Card(17);
            Assert.IsTrue(card.Suite == Card.Suites.Clubs && card.Value == Card.Values.Six, "The 17th card is not the Six of Clubs it is: {0}", card.ToString());
            card = new Card(36);
            Assert.IsTrue(card.Suite == Card.Suites.Diamonds && card.Value == Card.Values.Jack, "The 36th card is not the Queen of Hearts it is: {0}", card.ToString());
            card = new Card(51);
            Assert.IsTrue(card.Suite == Card.Suites.Spades && card.Value == Card.Values.Ace, "The last card is not the Ace of Spades it is: {0}", card.ToString());
            card = new Card(69);
            Assert.IsTrue(card.Suite == Card.Suites.Clubs && card.Value == Card.Values.Six, "The 69th card is not the Six of Clubs it is: {0}", card.ToString());
        }

        [TestMethod]
        public void CardCompareTests()
        {
 
            // compare ranks

            Card lowCard = new Card(5);     //Card Rank: 5 Name: Three of Clubs
            Card highCard = new Card(34);   //Card Rank: 34 Name: Ten of Hearts

            Assert.IsTrue((0 > lowCard.CompareTo(highCard)), "The lowCard:{0} is Ranked higher than the highCard: {1}", lowCard, highCard);
            Assert.IsFalse((0 == lowCard.CompareTo(highCard)), "The lowCard:{0} is Not equal to equalCard: {1}", lowCard, highCard);
            Assert.IsTrue((0 < highCard.CompareTo(lowCard)), "The highCard:{0} is Ranked lowwer than the lowCard: {1}", lowCard, highCard);
 
            // compare Values

            Card equalCard = new Card(7);       //Card Rank: 7 Name: Three of Spades 
            Assert.IsFalse((0 > equalCard.CompareTo(lowCard.Value)), "The Value of equalCard:{0} is less than the Value of lowCard: {1}", equalCard, lowCard);
            Assert.IsTrue((0 == equalCard.CompareTo(lowCard.Value)), "The Value of equalCard:{0} is Not equal to Value of lowCard: {1}", equalCard, lowCard);
            Assert.IsTrue((0 < highCard.CompareTo(equalCard.Value)), "The Value of HighCard:{0} is less than the Value of equalCard: {1}", highCard, equalCard);        

            // compare Suites

            Assert.IsTrue((0 > lowCard.CompareTo(equalCard.Suite)), "The Suite of lowCard:{0} is NOT less than the Suite of equalCard: {1}", lowCard, equalCard);
            Assert.IsTrue((0 != equalCard.CompareTo(lowCard.Suite)), "The Suite of equalCard:{0} is equal to Suite of lowCard: {1}", equalCard, lowCard);
            Assert.IsTrue((0 < equalCard.CompareTo(highCard.Suite)), "The Suite of equalCard:{0} is less than the Suite of highCard: {1}", equalCard, highCard);        

            // lot more conditions when time permits
        }

        [TestMethod]
        public void ListDeck()
        {
            foreach (Card c in BuildDeck())
            {
                Debug.WriteLine("Card Rank: {0} Name: {1}", c.Rank, c);
            }

        }
    }

}
