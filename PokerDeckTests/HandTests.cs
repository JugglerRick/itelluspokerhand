﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using PokerDeck;

// In general edge case testing dropped due to time constrants

namespace PokerDeckTests
{
    [TestClass]
    public class HandTests
    {

        private static Card[] highCards = 
        {
            new Card(50),   // Card Rank: 50 Name: Ace of Hearts (High card)
            new Card(2),    // Card Rank:  2 Name: Two of Hearts (lowest card)
            new Card(5),    // Card Rank:  5 Name: Three of Clubs 
            new Card(26),   // Card Rank: 26 Name: Eight of Hearts 
            new Card(33)    // Card Rank: 33 Name: Ten of Clubs
        };

        private static Card[] pairCards = 
        {
            new Card(51),   // Card Rank: 51 Name: Ace of Spades (High card)
            new Card(0),    // Card Rank: 0 Name: Two of Diamonds
            new Card(35),   // Card Rank: 35 Name: Ten of Spades
            new Card(40),   // Card Rank: 40 Name: Queen of Diamonds
            new Card(43)    // Card Rank: 43 Name: Queen of Spades
        };

        private static Card[] twoPairCards = 
        {
            new Card(38),   // Card Rank: 38 Name: Jack of Hearts
            new Card(37),   // Card Rank: 37 Name: Jack of Clubs
            new Card(48),   // Card Rank: 48 Name: Ace of Diamonds
            new Card(49),   // Card Rank: 49 Name: Ace of Clubs
            new Card(28)    // Card Rank: 28 Name: Nine of Diamonds
        };
        
        private static Card[] threeOfKindCards = 
        {
            new Card(3),    // Card Rank: 3 Name: Two of Spades
            new Card(45),   // Card Rank: 45 Name: King of Clubs
            new Card(46),   // Card Rank: 46 Name: King of Hearts
            new Card(47),   // Card Rank: 47 Name: King of Spades
            new Card(14)    // Card Rank: 14 Name: Five of Hearts
        };
        
        private Card[] straightCards = 
        {
            new Card(29),   // Card Rank: 29 Name: Nine of Clubs
            new Card(32),   // Card Rank: 32 Name: Ten of Diamonds
            new Card(36),   // Card Rank: 36 Name: Jack of Diamonds
            new Card(42),   // Card Rank: 42 Name: Queen of Hearts
            new Card(44)    // Card Rank: 44 Name: King of Diamonds
        };
        
        private Card[] flushCards = 
        {
            new Card(21),   // Card Rank: 21 Name: Seven of Clubs
            new Card(1),    // Card Rank:  1 Name: Two of Clubs
            new Card(13),   // Card Rank: 13 Name: Five of Clubs
            new Card(41),   // Card Rank: 41 Name: Queen of Clubs
            new Card(25)    // Card Rank: 25 Name: Eight of Clubs
        };

        private Card[] fullHouseCards =
        {
            new Card(45),    // Card Rank: 45 Name: King of Clubs
            new Card(46),    // Card Rank: 46 Name: King of Hearts
            new Card(47),    // Card Rank: 47 Name: King of Spades
            new Card(6),     // Card Rank:  6 Name: Three of Hearts
            new Card(7)      // Card Rank:  7 Name: Three of Spades
        }; 
        
        private Card[] fourOfKindCards =
        {
            new Card(8),    // Card Rank:  8 Name: Four of Diamonds
            new Card(9),    // Card Rank:  9 Name: Four of Clubs
            new Card(10),   // Card Rank: 10 Name: Four of Hearts
            new Card(11),   // Card Rank: 11 Name: Four of Spades
            new Card(36)    // Card Rank: 36 Name: Queen of Hearts
        };
        
        private Card[] straightFlushCards =
        {
            new Card(15),   // Card Rank: 15 Name: Five of Spades
            new Card(19),   // Card Rank: 19 Name: Six of Spades
            new Card(23),   // Card Rank: 23 Name: Seven of Spades
            new Card(27),   // Card Rank: 27 Name: Eight of Spades
            new Card(31)    // Card Rank: 31 Name: Nine of Spades
        };


        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConstructNullHandTest()
        {
            Hand hand = new Hand(null);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ConstructHandTooFewTest()        
        {
            Card[] cards =
            {
                new Card(42),   
                new Card(43),   
                new Card(45),   
                new Card(46),   
            };

            Hand hand = new Hand(cards);
        }


        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void ConstructHandTooManyTest()
        {
            Card[] cards =
            {
                new Card(42),   
                new Card(43),   
                new Card(44),   
                new Card(45),   
                new Card(46),   
                new Card(16),   
                new Card(24)    
            };

            Hand hand = new Hand(cards);
        }

        [TestMethod]
        public void FoldTest()
        {
            Hand hand = new Hand(highCards);
            Assert.IsTrue(hand.IsValid(), "The hand construction failed to create a valid Hand");
            hand.Fold();
            Assert.IsFalse(hand.IsValid(), "The Fold method did not clear the Hand");
        }

        [TestMethod]
        public void HighLowHandTest()
        {
            Hand hand = new Hand(highCards);

            Card highest = hand.HighestValue();
            Assert.IsTrue(50 == highest.Rank, $"{highest} is not the highest value in the hand"); 
            Card lowest = hand.LowestValue();
            Assert.IsTrue(2 == lowest.Rank, $"{lowest} is not the lowest test value in the hand"); 
        }

        [TestMethod]
        public void StraightHandTest()
        {
            Hand hand = new Hand(straightCards);
            Assert.IsTrue(hand.IsStraight(), "The straight hand is not found to be straight");
            hand = new Hand(highCards);
            Assert.IsFalse(hand.IsStraight(), "The crooked hand was found to be straight"); 
        }

        [TestMethod]
        public void FlushHandTest()
        {
            Hand hand = new Hand(flushCards);
            Assert.IsTrue(hand.IsFlush(), "The flushCards where not found to be flush");
            hand = new Hand(straightFlushCards);
            Assert.IsTrue(hand.IsFlush(), "The straightFlushCards where not found to be flush");
            hand = new Hand(highCards);
            Assert.IsFalse(hand.IsFlush(), "The highCards where found to be flush");
            
        }

        [TestMethod]
        public void RankHandTest()
        {
            Hand hand = new Hand(highCards);
            Assert.IsTrue(Hand.HandType.High == hand.Rank, "The High hand type was not detected");
            hand = new Hand(pairCards);
            Assert.IsTrue(Hand.HandType.Pair == hand.Rank, "The Pair hand type was not detected");
            hand = new Hand(twoPairCards);
            Assert.IsTrue(Hand.HandType.TwoPair == hand.Rank, "The TwoPair hand type was not detected");
            hand = new Hand(threeOfKindCards);
            Assert.IsTrue(Hand.HandType.ThreeOfKind == hand.Rank, "The ThreeOfKind hand type was not detected");
            hand = new Hand(straightCards);
            Assert.IsTrue(Hand.HandType.Straight == hand.Rank, "The Straight hand type was not detected");
            hand = new Hand(flushCards);
            Assert.IsTrue(Hand.HandType.Flush == hand.Rank, "The Flush hand type was not detected");
            hand = new Hand(fullHouseCards);
            Assert.IsTrue(Hand.HandType.FullHouse == hand.Rank, "The FullHouse hand type was not detected");
            hand = new Hand(fourOfKindCards);
            Assert.IsTrue(Hand.HandType.FourOfKind == hand.Rank, "The StraightFlush hand type was not detected");
            hand = new Hand(straightFlushCards);
            Assert.IsTrue(Hand.HandType.StraightFlush == hand.Rank, "The StraightFlush hand type was not detected");           
        }

        [TestMethod]
        public void CompareRankHandsTest()
        {
            Hand hand1 = new Hand(highCards);
            Hand hand2 = new Hand(pairCards);
            // test 2 different ranks
              
            Assert.IsTrue(0 > hand1.CompareTo(hand2), "The hand ranking compare test has failed");
        }       

        // test of pair of high hands
        [TestMethod]
        public void CompareHighHandsTest()
        {
            //this is higher than highCards hand
            Card[] cards = 
            {
                new Card(51),   // Card Rank: 51 Name: Ace of Spades (High card)
                new Card(0),    // Card Rank: 0 Name: Two of Diamonds
                new Card(35),   // Card Rank: 35 Name: Ten of Spades
                new Card(40),   // Card Rank: 40 Name: Queen of Diamonds
                new Card(6)     // Card Rank: 6 Name: Three of Hearts
            };
            Hand hand1 = new Hand(highCards);
            Hand hand2 = new Hand(cards);
              
            Assert.IsTrue(0 > hand1.CompareTo(hand2), "The CompareHighHandsTest test has failed");
            
        }

        // test of pair of pairs of kind
        [TestMethod]
        public void ComparePairsTest()
        {
            // this is worst case both pairs are of equal value 
            Card[] cards = 
            {
                new Card(41),   // Card Rank: 41 Name: Queen of Clubs
                new Card(42),   // Card Rank: 42 Name: Queen of Hearts
                new Card(18),   // Card Rank: 18 Name: Six of Hearts
                new Card(49),   // Card Rank: 49 Name: Ace of Clubs
                new Card(28)    // Card Rank: 28 Name: Nine of Diamonds
            };
            Hand hand1 = new Hand(pairCards);
            Hand hand2 = new Hand(cards);

            Assert.IsTrue(0 < hand1.CompareTo(hand2), "The ComparePairsTest test has failed");
            
        }

        // test a pair of flushs
        [TestMethod]
        public void CompareFlushTest()
        {
            // this is a bit of an edge case because both hand have queens has high card
            Card[] cards = 
            {
                new Card(6),    // Card Rank: 6 Name: Three of Hearts
                new Card(42),   // Card Rank: 42 Name: Queen of Hearts
                new Card(18),   // Card Rank: 18 Name: Six of Hearts
                new Card(22),   // Card Rank: 22 Name: Seven of Hearts
                new Card(34)    // Card Rank: 34 Name: Ten of Hearts
            };
            Hand hand1 = new Hand(flushCards);
            Hand hand2 = new Hand(cards);

            Assert.IsTrue(0 > hand1.CompareTo(hand2), "The CompareFlushTest test has failed");
        }

        // test a pair of straights
        [TestMethod]
        public void CompareStraightsTest()
        {
            Card[] cards = 
            {
                new Card(6),    // Card Rank:  6 Name: Three of Hearts
                new Card(10),   // Card Rank: 10 Name: Four of Hearts
                new Card(12),   // Card Rank: 12 Name: Five of Diamonds
                new Card(18),   // Card Rank: 18 Name: Six of Hearts
                new Card(22)    // Card Rank: 22 Name: Seven of Hearts
            };
            Hand hand1 = new Hand(straightCards);
            Hand hand2 = new Hand(cards);

            Assert.IsTrue(0 < hand1.CompareTo(hand2), "The CompareStraightsTest test has failed");
        }
    }
}
