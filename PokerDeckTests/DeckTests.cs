﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PokerDeck;

namespace PokerDeckTests
{
    [TestClass]
    public class DeckTests
    {
        [TestMethod]
        public void DeckBuildTest()
        {
            Deck deck = new Deck();
            for(int i = 0; i < Deck.NUMBER_OF_CARDS; ++i)
            {
                Card top = deck.DealCard();
                Assert.AreEqual(i, top.Rank, "The deck was not constructed in the proper order");
            }
        }

        [TestMethod]
        public void DeckShuffleTest()
        {
            Deck deck = new Deck();
            deck.Shuffle();
            int count = 0;
            Card top = deck.DealCard();
            Card next = deck.DealCard();
            while(count < Deck.NUMBER_OF_CARDS && next.Rank -1 == top.Rank)
            {
                ++count;
                top = next;
                next = deck.DealCard();
            }
            Assert.IsTrue(count < Deck.NUMBER_OF_CARDS - 1, "The deck was not randomized");
        }
    }
}
