This project was developed using Visual Studio 2017.
I chose the Poker Hand domain.
The cards are represented by a simple integer value that determines there rank. 
The card face value is determined by dividing the rank by number of suites and suite is the rank modulo by the number of suites.
This structure allow the suite to be included in the ranking of a card basically for free so, I have included it.
