﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PokerDeck;

namespace PokerDeckGui
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Player player1;
        private Player player2;
        private List<TextBlock> player1HandDisplayList;
        private List<TextBlock> player2HandDisplayList;
        private Brush winningColor;
        private Brush losingColor;

        public MainWindow()
        {
            InitializeComponent();
            player1 = new Player();
            player1HandDisplayList = new List<TextBlock>(Hand.NUMBER_OF_CARDS);
            player1HandDisplayList.Add(hand1card1);
            player1HandDisplayList.Add(hand1card2);
            player1HandDisplayList.Add(hand1card3);
            player1HandDisplayList.Add(hand1card4);
            player1HandDisplayList.Add(hand1card5);
            player2 = new Player();
            player2HandDisplayList = new List<TextBlock>(Hand.NUMBER_OF_CARDS);
            player2HandDisplayList.Add(hand2card1);
            player2HandDisplayList.Add(hand2card2);
            player2HandDisplayList.Add(hand2card3);
            player2HandDisplayList.Add(hand2card4);
            player2HandDisplayList.Add(hand2card5);

            winningColor = (Brush)FindResource("WinningBrush");
            losingColor = (Brush)FindResource("LosingBrush");
        }

        private void DealHand_Click(object sender, RoutedEventArgs e)
        {
            player1.Hand.Fold();
            player2.Hand.Fold();

            Deck deck = new Deck();
            deck.Shuffle();
            for(int i = 0; i < Hand.NUMBER_OF_CARDS; ++i)
            {
                player1.DealtCard(deck.DealCard());
                player2.DealtCard(deck.DealCard());
            }
            for (int i = 0; i < Hand.NUMBER_OF_CARDS; ++i)
            {
                player1HandDisplayList[i].Text = player1.Hand.Cards[i].ToString();
                player2HandDisplayList[i].Text = player2.Hand.Cards[i].ToString();
            }
            if (0 < player1.CompareTo(player2))
            {
                player1Hand.Background = winningColor;
                player2Hand.Background = losingColor;
            }
            else
            {
                player1Hand.Background = losingColor;
                player2Hand.Background = winningColor;
            }

        }

        private void Player1Name_LostFocus(object sender, RoutedEventArgs e)
        {
            player1.Name = player1Name.Text;
        }

        private void Player2Name_LostFocus(object sender, RoutedEventArgs e)
        {
            player2.Name = player2Name.Text;
        }
    }
}
